package com.cnn.drei.http;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.SelfSignedCertificate;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;



/**
 * An HTTP server that sends back the content of the received HTTP request
 * in a pretty plaintext form.
 */
public final class HttpServer {

    static final boolean SSL = System.getProperty("ssl") != null;
    static final boolean LOAD_STATIC_JSON_FILE = System.getProperty("staticJsonFile") != null;
    static final int PORT = Integer.parseInt(System.getProperty("port", SSL? "8443" : "8080"));
    static final String FILE_NAME = System.getProperty("staticJsonFile");
    final static Charset ENCODING = StandardCharsets.UTF_8;
    static String JSON_FILE = null;
    
    public static void preloadFile() throws IOException {
    	Path fFilePath = Paths.get(FILE_NAME);
    	try (Scanner scanner =  new Scanner(fFilePath, ENCODING.name())) {
    		StringBuilder stringBuilder = new StringBuilder();
    		while (scanner.hasNextLine()) {
    			stringBuilder.append(scanner.nextLine());
    		}
    		JSON_FILE = stringBuilder.toString();
    	}
    }

    public static void main(String[] args) throws Exception {
        // Configure SSL.
        final SslContext sslCtx;
        if (SSL) {
            SelfSignedCertificate ssc = new SelfSignedCertificate();
            sslCtx = SslContext.newServerContext(ssc.certificate(), ssc.privateKey());
        } else {
            sslCtx = null;
        }
        
        /**
         * Preload JSON File into memory.
         */
        if (LOAD_STATIC_JSON_FILE) {
        	preloadFile();
        }
        // Configure the server.
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.option(ChannelOption.SO_BACKLOG, 1024);
            b.group(bossGroup, workerGroup)
             .channel(NioServerSocketChannel.class)
             .handler(new LoggingHandler(LogLevel.INFO))
             .childHandler(new HttpServerInitializer(sslCtx, JSON_FILE));

            Channel ch = b.bind(PORT).sync().channel();

            System.err.println("Open your web browser and navigate to " +
                    (SSL? "https" : "http") + "://127.0.0.1:" + PORT + '/');

            ch.closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}
