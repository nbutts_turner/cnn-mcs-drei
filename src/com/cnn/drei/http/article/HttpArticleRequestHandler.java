package com.cnn.drei.http.article;

import java.io.IOException;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;

import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.conn.SchemePortResolver;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
// import io.netty.handler.codec.http.HttpResponse;
// 	import io.netty.util.concurrent.Future;
import static io.netty.handler.codec.http.HttpHeaders.Names.*;
import static io.netty.handler.codec.http.HttpHeaders.Values;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.netty.handler.codec.http.HttpVersion.*;

public class HttpArticleRequestHandler {
    private static final String ARTICLE_URL_STRING = "http://capi-hypatia-fe.ref.56m.dmtio.net/svc/content/v2/search/collection1/dataSource:cnn/id:";
    CloseableHttpAsyncClient asyncHttpClient;
    
    public HttpArticleRequestHandler() {}
    
    public byte[] run(String articleId) {
    	byte[] responseEntity = null;
    	BasicResponseHandler handler = new BasicResponseHandler();
		HttpGet httpget = new HttpGet(ARTICLE_URL_STRING + articleId);
		System.out.println("ARTICLE_URL_STRING + articleId" + ARTICLE_URL_STRING + articleId);
		asyncHttpClient = HttpAsyncClients.createDefault();
		try {
			asyncHttpClient.start();
			Future<HttpResponse> future = asyncHttpClient.execute(httpget, null);
			HttpResponse response = future.get();
			responseEntity = handler.handleResponse(response).getBytes();
			System.out.println("responseEntity:" + responseEntity);
		} catch(Exception e) {
			System.out.println("Exception caught");
		} finally {
			try {
				asyncHttpClient.close();
			} catch (Exception e) {} 
		}
        return responseEntity;
    }
}