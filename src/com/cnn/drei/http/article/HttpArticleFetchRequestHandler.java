package com.cnn.drei.http.article;

import java.awt.List;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import static io.netty.handler.codec.http.HttpHeaders.Names.*;
import static io.netty.handler.codec.http.HttpHeaders.Values;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.netty.handler.codec.http.HttpVersion.*;

public class HttpArticleFetchRequestHandler {
    private static final String ARTICLELIST_URL_STRING = "http://capi-hypatia-fe.ref.56m.dmtio.net/svc/content/v2/search/collection1/dataSource:cnn/rows:100";
    private static ObjectMapper MAPPER = new ObjectMapper();
    private Map<String, String> articles = new HashMap();
	private StringBuilder stringBuilder = new StringBuilder();   
	int listSize = 0;
    
    public HttpArticleFetchRequestHandler() {}
    
    public byte[] run(int limit) throws Exception {
    	listSize = limit;
   
    	RequestConfig requestConfig = RequestConfig.custom()
    			.setSocketTimeout(3000)
    			.setConnectTimeout(3000).build();
    	CloseableHttpAsyncClient httpclient = HttpAsyncClients.custom()
    			.setDefaultRequestConfig(requestConfig)
    			.build();
    	try {
    		httpclient.start();
    		double requestIndex = (double)limit/100;
    		int period = (int)Math.ceil(requestIndex);
    		final HttpGet[] requests = new HttpGet[period];
    		for (int i = 0; i < period; i++) {
    			requests[i] = new HttpGet(ARTICLELIST_URL_STRING + "/start:" + i*100);
    		}
    		final CountDownLatch latch = new CountDownLatch(requests.length);
    		stringBuilder.append("<ol>");
    		for (final HttpGet request: requests) {
    			httpclient.execute(request, new FutureCallback<HttpResponse>() {
    				public void completed(final HttpResponse response) {
    					BasicResponseHandler handler = new BasicResponseHandler();
    					latch.countDown();
    					stringBuilder.append("<li>" + request.getRequestLine() + "->" + response.getStatusLine() + "</li>");
    					System.out.println(request.getRequestLine() + "->" + response.getStatusLine());
    				}
    				public void failed(final Exception ex) {
    					latch.countDown();
    					System.out.println(request.getRequestLine() + "->" + ex);
    				}
    				public void cancelled() {
    					latch.countDown();
    					System.out.println(request.getRequestLine() + "cancelled");
    				}
    			});
    		}
    		latch.await();
    		System.out.println("Shutting down");
    	} finally {
    		try {
    			httpclient.close();
    		} catch(Exception e) {}
    	}
    	System.out.println("Done");
    	stringBuilder.append("</ol>");
        return stringBuilder.toString().getBytes();
    } 
}