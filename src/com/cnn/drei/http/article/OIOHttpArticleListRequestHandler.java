package com.cnn.drei.http.article;

import java.awt.List;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

import com.cnn.drei.item.BasicItem;

public class OIOHttpArticleListRequestHandler {
    private static final byte[] CONTENT = { 'J', 'S', 'O', 'N' };
    private static final String ARTICLELIST_URL_STRING = "http://capi-hypatia-fe.ref.56m.dmtio.net/svc/content/v2/search/collection1/dataSource:cnn/rows:100";
    public HttpClient httpClient = new DefaultHttpClient();
    private static ObjectMapper MAPPER = new ObjectMapper();
    private ArrayList<BasicItem> articles = new ArrayList<BasicItem>();
    
    public OIOHttpArticleListRequestHandler() {}
    
    public byte[] run(int limit) throws Exception {
    	int start = 0;
    	double requestIndex = (double)limit/100;
		int end = (int)Math.ceil(requestIndex)*100;
    	for (int i = start; i <= end; i+=100) {
    		extract(getHttpDocument(ARTICLELIST_URL_STRING + "/start:" + i));
    	}
    	ObjectMapper jsonOutputMapper = new ObjectMapper();
    	return jsonOutputMapper.writeValueAsString(articles).getBytes();
    }
    
    public StringBuilder extract(String jsonText) {
    	StringBuilder responseStringBuilder = new StringBuilder();
    	JsonNode rootNode = null;
		if (jsonText != null) {
			try {
				rootNode = MAPPER.readTree(jsonText);
			}
			catch (JsonParseException e) {
				System.out.println("Found error while trying to parse the JSON retrieved. Probably not JSON. url = ");
			}
			catch (JsonProcessingException e) {
				System.out.println("Found JSON error while trying to process the JSON retrieved. url = ");
			}
			catch (IOException e) {
				System.out.println("Found I/O error while trying to parse the JSON retrieved.");
			}
		}
		
		if (rootNode != null && rootNode.has("docs") && rootNode.get("docs").isArray()) {
			ArrayNode docs = (ArrayNode) rootNode.get("docs");
			for (JsonNode d : docs) {
				articles.add(new BasicItem(d.path("id").getTextValue(), d.path("url").getTextValue()));
			}
		}
		
		return responseStringBuilder;
    } 
    
    public synchronized String getHttpDocument(String url) {
    	// System.out.println("Getting ...... " + url);
		BasicResponseHandler handler = new BasicResponseHandler();
		HttpGet httpget = new HttpGet(url);
		String responseData = null;
		try {
			responseData = httpClient.execute(httpget, handler);
		}
		catch (HttpResponseException e) {
			System.out.println("HttpResponseException while trying to get = " + url + ", http status code = " + e.getStatusCode());
			// TODO: handle http response codes >= 300 here.
			switch (e.getStatusCode()) {
				case HttpServletResponse.SC_NOT_MODIFIED:
					System.out.println("Response for url " + url + " is not modified (HTTP Response code 304).");
				case HttpServletResponse.SC_NOT_FOUND:
					System.out.println("Url " + url + " is not found (HTTP Response code 404).");
				case HttpServletResponse.SC_BAD_REQUEST:
					System.out.println("Response for url " + url + " is not modified (HTTP Response code 400).");
				case HttpServletResponse.SC_INTERNAL_SERVER_ERROR:
					System.out.println("Url " + url + " caused a server error (HTTP Response code 500).");
				default:
					return null;
			}
		}
		catch (IOException e) {
			System.out.println("Exception while trying to get = " + url);
			return null;
		}
		return responseData; //.getBytes();
	}
}