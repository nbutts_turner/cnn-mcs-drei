package com.cnn.drei.http.marshaller;

import java.io.IOException;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

public class HttpJsonMarshallHandler {
	
	private static final byte[] CONTENT = { 'M', 'A', 'R', 'S', 'H', 'A', 'L', 'L','E','D' };
    private static ObjectMapper MAPPER = new ObjectMapper();
    private static String jsonContent;
    
    public HttpJsonMarshallHandler(String content) {
    	jsonContent = content;
    }
    
    public byte[] run() {
        return (getJsonDocument() != null)? CONTENT : null;
    }
    
    public JsonNode getJsonDocument() {
    	JsonNode rootNode = null;
		if (jsonContent != null) {
			try {
				rootNode = MAPPER.readTree(jsonContent);
			}
			catch (JsonParseException e) {
				System.out.println("Found error while trying to parse the JSON retrieved. Probably not JSON. url = ");
			}
			catch (JsonProcessingException e) {
				System.out.println("Found JSON error while trying to process the JSON retrieved. url = ");
			}
			catch (IOException e) {
				System.out.println("Found I/O error while trying to parse the JSON retrieved.");
			}
		}
		
		return rootNode;
    } 
}