package com.cnn.drei.http;

import java.util.List;
import java.util.Map;

import com.cnn.drei.http.article.HttpArticleFetchRequestHandler;
import com.cnn.drei.http.article.HttpArticleListRequestHandler;
import com.cnn.drei.http.article.HttpArticleRequestHandler;
import com.cnn.drei.http.article.OIOHttpArticleListRequestHandler;
import com.cnn.drei.http.helloworld.HttpHelloWorldServerHandler;
import com.cnn.drei.http.marshaller.HttpJsonMarshallHandler;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.QueryStringDecoder;
import static io.netty.handler.codec.http.HttpHeaders.Names.*;
import static io.netty.handler.codec.http.HttpHeaders.Values;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.netty.handler.codec.http.HttpVersion.*;

public class HttpServerHandler extends ChannelInboundHandlerAdapter {
	
	private String jsonContent;

	public HttpServerHandler(String content) {
		jsonContent = content;
	}
	
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
    	byte[] responseAsByte = {'E', 'M', 'P', 'T', 'Y'};
    	byte[] defaultResponseAsByte = {'D', 'E', 'F', 'A', 'U', 'L', 'T'};
    	String contentType = "text/html";
    	
    	System.out.println("Request received...");
    	
    	if (msg instanceof HttpRequest) {
    		HttpRequest req = (HttpRequest) msg;
    		QueryStringDecoder decoder = new QueryStringDecoder(req.getUri());
            
            if (HttpHeaders.is100ContinueExpected(req)) {
                ctx.write(new DefaultFullHttpResponse(HTTP_1_1, CONTINUE));
            }
            boolean keepAlive = HttpHeaders.isKeepAlive(req);
            String path = decoder.path();
            if (path.equals("/hello")) {
            	responseAsByte = new HttpHelloWorldServerHandler().run();
            } else if (path.contains("/article/")) {  
            	responseAsByte = new HttpArticleRequestHandler().run(path.substring(path.lastIndexOf("/")+1));
            	contentType = "application/json";
            } else if (path.equals("/articles/fetch")) {
            	System.out.println("in /articles/fetch");
            	int articleLimit = 10;
            	
            	if (decoder.parameters().containsKey("q")) {
            		articleLimit = Integer.parseInt(decoder.parameters().get("q").get(0));
            	}
            	try {
            		responseAsByte = new HttpArticleFetchRequestHandler().run(articleLimit);
            	} catch (Exception e) {}
            } else if (path.equals("/articles/list")) {
            	System.out.println("in /articles/list");
            	int articleLimit = 10;
            	if (decoder.parameters().containsKey("q")) {
            		articleLimit = Integer.parseInt(decoder.parameters().get("q").get(0));
            	}
            	if (decoder.parameters().containsKey("oio")) {
            		try {
                		responseAsByte = new OIOHttpArticleListRequestHandler().run(articleLimit);
                	} catch (Exception e) {}
            	} else {
            		try {
                		responseAsByte = new HttpArticleListRequestHandler().run(articleLimit);
                	} catch (Exception e) {}
            	}
            	contentType = "application/json";
            } else if (decoder.path().equals("/marshaller")) {
            	responseAsByte = new HttpJsonMarshallHandler(jsonContent).run();
            	if (responseAsByte != null) { 
            		ctx.writeAndFlush(new DefaultFullHttpResponse(HTTP_1_1, OK)).addListener(ChannelFutureListener.CLOSE);
            	}
            	else {
            		ctx.writeAndFlush(new DefaultFullHttpResponse(HTTP_1_1, INTERNAL_SERVER_ERROR)).addListener(ChannelFutureListener.CLOSE);
            	}
            	return;
            }
            
            FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, OK, Unpooled.wrappedBuffer((responseAsByte != null)? responseAsByte : defaultResponseAsByte));
            response.headers().set(CONTENT_TYPE, contentType);
            response.headers().set(CONTENT_LENGTH, response.content().readableBytes());
            
            if (!keepAlive) {
                ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
            } else {
                response.headers().set(CONNECTION, Values.KEEP_ALIVE);
                ctx.write(response);
            }
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}