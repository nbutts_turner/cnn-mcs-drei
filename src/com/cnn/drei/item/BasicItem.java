package com.cnn.drei.item;

public class BasicItem {

	private String id;
	private String url;
	
	public BasicItem(String id, String url) {
		this.id = id;
		this.url = url;
	} 
	
	public void setId(String id) {
		this.id = id;
	} 
	
	public String getId() {
		return this.id;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getUrl() {
		return this.url;
	}
}
